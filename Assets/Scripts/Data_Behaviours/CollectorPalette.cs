using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "New Collector Palette", menuName = "ScriptableObjects/Collector Palette")]
public class CollectorPalette : ScriptableObject
{
    public Dictionary<ShapeID, GameObject> CollectorData = new Dictionary<ShapeID, GameObject>();

    [SerializeField] List<ShapeID> shapeIDs;
    [SerializeField] List<GameObject> objects;

    public void Initialize()
    {
        if (shapeIDs.Count == objects.Count)
        {
            for (int i = 0; i < shapeIDs.Count; i++)
            {
                CollectorData.Add(shapeIDs[i], objects[i]);
            }
        }
        else
        {
            Debug.LogError("Key and Value Counts Not Equal");
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(CollectorPalette))]
public class CollectorPaletteEditor : Editor
{
    CollectorPalette collectorPalette;

    SerializedProperty shapeID;
    SerializedProperty obj;

    private void OnEnable()
    {
        collectorPalette = (CollectorPalette)target;
        shapeID = serializedObject.FindProperty("shapeIDs");
        obj = serializedObject.FindProperty("objects");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.BeginHorizontal();

        EditorGUILayout.PropertyField(shapeID);
        EditorGUILayout.PropertyField(obj);

        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif