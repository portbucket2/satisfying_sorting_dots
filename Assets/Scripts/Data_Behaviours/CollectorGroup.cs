using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;

[CreateAssetMenu(fileName = "New Collector Group", menuName = "ScriptableObjects/Collector Group")]
public class CollectorGroup : ScriptableObject
{
    [System.Serializable]
    public class CollectorGroupData
    {
        public ColorID colorID;
        public ShapeID shapeID;
        public float3 position;

        public GameObject GetShapedCollector()
        {
            return GameData.Instance.collectorPalette.CollectorData[shapeID];
        }

        public Color GetCollectorColor()
        {
            return GameData.Instance.colorPalette.ColorData[colorID];
        }
    }

    public List<CollectorGroupData> collectors;

    public CollectorGroupData GetMatchingCollector(ShapeID shape, ColorID color, bool matchBoth = false)
    {
        CollectorGroupData matchedCollector = null;

        if (!matchBoth)
        {
            foreach (CollectorGroupData collector in collectors)
            {
                if (shape == collector.shapeID || color == collector.colorID)
                {
                    matchedCollector = collector;
                    break;
                }
            }
        }

        return matchedCollector;
    }
}
