﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Object Group", menuName = "ScriptableObjects/Object Group")]
public class ObjectGroup : ScriptableObject
{
    [System.Serializable]
    public struct ObjectGroupData
    {
        public ColorID colorID;
        public ShapeID shapeID;
        [Range(0f, 1f)]
        public float portionInCluster;

        public GameObject GetShapedObject()
        {
            return GameData.Instance.objectPalette.ObjectData[shapeID];
        }

        public Color GetObjectColor()
        {
            return GameData.Instance.colorPalette.ColorData[colorID];
        }
    }

    public List<ObjectGroupData> objectGroups;
}
