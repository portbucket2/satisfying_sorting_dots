﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "New Object Palette", menuName = "ScriptableObjects/Object Palette")]
public class ObjectPalette : ScriptableObject
{
    public Dictionary<ShapeID, GameObject> ObjectData = new Dictionary<ShapeID, GameObject>();

    [SerializeField] List<ShapeID> shapeIDs;
    [SerializeField] List<GameObject> objects;

    public void Initialize()
    {
        if (shapeIDs.Count == objects.Count)
        {
            for (int i = 0; i < shapeIDs.Count; i++)
            {
                ObjectData.Add(shapeIDs[i], objects[i]);
            }
        }
        else
        {
            Debug.LogError("Key and Value Counts Not Equal");
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(ObjectPalette))]
public class ObjectPaletteEditor : Editor
{
    ObjectPalette objectPalette;

    SerializedProperty shapeID;
    SerializedProperty obj;

    private void OnEnable()
    {
        objectPalette = (ObjectPalette)target;
        shapeID = serializedObject.FindProperty("shapeIDs");
        obj = serializedObject.FindProperty("objects");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.BeginHorizontal();

        EditorGUILayout.PropertyField(shapeID);
        EditorGUILayout.PropertyField(obj);

        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
}
#endif