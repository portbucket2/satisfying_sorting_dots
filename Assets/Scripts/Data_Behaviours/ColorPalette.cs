﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CreateAssetMenu(fileName = "New Color Palette", menuName = "ScriptableObjects/Color Palette")]
public class ColorPalette : ScriptableObject
{
    public Dictionary<ColorID, Color> ColorData = new Dictionary<ColorID, Color>();

    [SerializeField] List<ColorID> colorIDs;
    [SerializeField] List<Color> colors;

    public void Initialize()
    {
        if (colorIDs.Count == colors.Count)
        {
            for (int i = 0; i < colorIDs.Count; i++)
            {
                ColorData.Add(colorIDs[i], colors[i]);
            } 
        }
        else
        {
            Debug.LogError("Key and Value Counts Not Equal");
        }
    }
}


#if UNITY_EDITOR
[CustomEditor(typeof(ColorPalette))]
public class ColorPaletteEditor : Editor
{
    ColorPalette colorPalette;

    SerializedProperty colorID;
    SerializedProperty color;

    private void OnEnable()
    {
        colorPalette = (ColorPalette)target;
        colorID = serializedObject.FindProperty("colorIDs");
        color = serializedObject.FindProperty("colors");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();

        GUILayout.BeginHorizontal();

        EditorGUILayout.PropertyField(colorID);
        EditorGUILayout.PropertyField(color);

        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }
} 
#endif