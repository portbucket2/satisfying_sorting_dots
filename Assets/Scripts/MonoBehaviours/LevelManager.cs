using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Rendering;

public class LevelManager : MonoBehaviour
{
    public static LevelManager Instance { get; private set; }
    public static EntityManager entityManager;

    [Header("References:")]
    public UIManager uiManager;

    [Header("Gameplay Settings:")]
    public int units;
    public bool matchBoth;
    public ObjectGroup objectGroup;
    public CollectorGroup collectorGroup;

    [SerializeField] MyUtils.MinMaxFloat xBounds;
    [SerializeField] MyUtils.MinMaxFloat yRange;
    [SerializeField] MyUtils.MinMaxFloat zBounds;

    public event Action OnLevelStart;
    BlobAssetStore blob;

    private void Awake()
    {
        Instance = this;
        entityManager = World.DefaultGameObjectInjectionWorld.EntityManager;
        OnLevelStart += GameStart;
    }

    // Start is called before the first frame update
    void Start()
    {
        OnLevelStart?.Invoke();
    }

    void GameStart()
    {
        blob = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blob);

        for (int i = 0; i < collectorGroup.collectors.Count; i++)
        {
            Entity entityToSpawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(collectorGroup.collectors[i].GetShapedCollector(), settings);
            ColorID colorID = collectorGroup.collectors[i].colorID;
            ShapeID shapeID = collectorGroup.collectors[i].shapeID;
            Color color = collectorGroup.collectors[i].GetCollectorColor();

            Entity spawnedEntity = entityManager.Instantiate(entityToSpawn);
            float3 spawnPoint = collectorGroup.collectors[i].position;

            entityManager.SetComponentData(spawnedEntity, new Translation { Value = spawnPoint });
            entityManager.SetComponentData(spawnedEntity, new URPMaterialPropertyBaseColor { Value = new float4(color.r, color.g, color.b, color.a) });

            entityManager.AddComponentData(spawnedEntity, new CollectorData { colorID = colorID, shapeID = shapeID });
        }

        for (int i = 0; i < objectGroup.objectGroups.Count; i++)
        {
            int unitsToSpawn = (int)(units * objectGroup.objectGroups[i].portionInCluster);
            Entity entityToSpawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(objectGroup.objectGroups[i].GetShapedObject(), settings);
            ColorID colorID = objectGroup.objectGroups[i].colorID;
            ShapeID shapeID = objectGroup.objectGroups[i].shapeID;
            Color color = objectGroup.objectGroups[i].GetObjectColor();

            bool addCollector = false;
            float3 collectorPos = float3.zero;

            CollectorGroup.CollectorGroupData matchedCollector = collectorGroup.GetMatchingCollector(shapeID, colorID, matchBoth);

            if (matchedCollector != null)
            {
                addCollector = true;
                collectorPos = matchedCollector.position;
            }

            for (int j = 0; j < unitsToSpawn; j++)
            {
                Entity spawnedEntity = entityManager.Instantiate(entityToSpawn);
                float3 spawnPoint = new float3(xBounds.GetRandomInRange(), yRange.GetRandomInRange(), zBounds.GetRandomInRange());
                float3 randRotation = new float3(UnityEngine.Random.Range(-180f, 180f), UnityEngine.Random.Range(-180f, 180f), UnityEngine.Random.Range(-180f, 180f));

                entityManager.SetComponentData(spawnedEntity, new Translation { Value = spawnPoint });
                entityManager.SetComponentData(spawnedEntity, new Rotation { Value = quaternion.Euler(randRotation) });
                entityManager.SetComponentData(spawnedEntity, new URPMaterialPropertyBaseColor { Value = new float4(color.r, color.g, color.b, color.a) });

                entityManager.AddComponentData(spawnedEntity, new PhysicalBody { colorID = colorID, shapeID = shapeID, collectable = addCollector,
                    collectorPos = collectorPos });
            }
        }
    }

    public void LevelComplete()
    {
        //Level Complete Event:
        //MySDKManager.LogLevelComplete(GameManager.Instance.CurrentLevel);

        GameManager.Instance.IncreaseLevel();
        //confetti_PS.gameObject.SetActive(true);
        //confetti_PS.Play();
        LeanTween.delayedCall(2f, () => uiManager.ShowLevelCompleteUI());
    }

    public void LevelFailed()
    {
        //Level Failed Event:
        //MySDKManager.LogLevelFailed(GameManager.Instance.CurrentLevel);
    }

    public void GotoNextLevel()
    {
        LeanTween.cancelAll();
        SceneManager.LoadScene(GameManager.Instance.SceneToLoad);
    }

    public void RetryLevel()
    {
        //Level Restart Event:
        //MySDKManager.LogLevelRestart(GameManager.Instance.CurrentLevel);

        LeanTween.cancelAll();
        SceneManager.LoadScene(GameManager.Instance.SceneToLoad);
    }

    private void OnDestroy()
    {
        if (blob != null)
        {
            blob.Dispose();
        }
    }
}
