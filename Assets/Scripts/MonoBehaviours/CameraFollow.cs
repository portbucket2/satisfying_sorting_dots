using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    float frustumWidth;
    float frustumHeight;
    Camera cam;

    [Range(0f, 1f)] [SerializeField] float widthContainmentRatio;
    [Range(0f, 1f)] [SerializeField] float heightContainmentRatio;

    float widthContainment;
    float heightContainment;

    float viewDistance;

    //[SerializeField] float smoothFollowSpeed;

    // Start is called before the first frame update
    IEnumerator Start()
    {
        yield return new WaitForSeconds(2f);
        cam = GetComponent<Camera>();
        viewDistance = Vector3.Distance(transform.position, MagnetManager.Instance.magnetPos);
        frustumHeight = 2.0f * viewDistance * Mathf.Tan(cam.fieldOfView * 0.5f * Mathf.Deg2Rad);
        frustumWidth = frustumHeight * cam.aspect;

        widthContainment = (frustumWidth / 2f) * widthContainmentRatio;
        heightContainment = (frustumHeight / 2f) * heightContainmentRatio;
    }

    // Update is called once per frame
    void LateUpdate()
    {
        Vector3 viewPoint = transform.position + transform.forward * viewDistance;
        Vector3 connection = new Vector3(MagnetManager.Instance.magnetPos.x - viewPoint.x,
            MagnetManager.Instance.magnetPos.y - viewPoint.y, MagnetManager.Instance.magnetPos.z - viewPoint.z);

        if (Mathf.Abs(connection.x) > widthContainment)
        {
            float delta = Mathf.Abs(connection.x) - widthContainment;
            Vector3 targetPos = transform.position + Vector3.right * Mathf.Sign(connection.x) * delta;
            //transform.position = Vector3.Lerp(transform.position, targetPos, smoothFollowSpeed * Time.deltaTime);
            transform.position = targetPos;
        }

        if (Mathf.Abs(connection.y) > heightContainment)
        {
            float delta = Mathf.Abs(connection.y) - heightContainment;
            Vector3 targetPos = transform.position + Vector3.up * Mathf.Sign(connection.y) * delta;
            //transform.position = Vector3.Lerp(transform.position, targetPos, smoothFollowSpeed * Time.deltaTime);
            transform.position = targetPos;
        }
    }
}
