using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Transforms;
using Unity.Mathematics;
using Unity.Physics;

public class MagnetManager : MonoBehaviour
{
    public static MagnetManager Instance { get; private set; }

    public bool magnetized { get; set; }
    public float3 magnetPos { get; set; }

    public float G_Magnet;
    public float G_Collector;

    public ColorID colorID;
    public ShapeID shapeID;

    [SerializeField] float magnetMoveSpeed;

    Entity magnetEntity;

    BlobAssetStore blob;

    [SerializeField] GameObject magnet_Prefab;

    [Header("Settings:")]
    public bool magnetChange;
    public float magnetChangeInterval;
    public float transitionDuration;

    private void Awake()
    {
        Instance = this;
        LevelManager.Instance.OnLevelStart += GameStart;
    }

    // Start is called before the first frame update
    void Start()
    {
    }

    void GameStart()
    {
        blob = new BlobAssetStore();
        var settings = GameObjectConversionSettings.FromWorld(World.DefaultGameObjectInjectionWorld, blob);
        var entityToSpawn = GameObjectConversionUtility.ConvertGameObjectHierarchy(magnet_Prefab, settings);

        float3 spawnPoint = new float3(0f, 10f, 0f);

        magnetEntity = LevelManager.entityManager.Instantiate(entityToSpawn);
        LevelManager.entityManager.SetComponentData(magnetEntity, new Translation { Value = spawnPoint });
        LevelManager.entityManager.AddComponentData(magnetEntity, new MagnetData { moveSpeed = magnetMoveSpeed });
        LeanTween.delayedCall(2f, () => magnetized = true);
        magnetPos = spawnPoint;
    }

    public void ColorTransition()
    {

    }

    private void OnDestroy()
    {
        if (blob != null)
        {
            blob.Dispose();
        }
    }
}
