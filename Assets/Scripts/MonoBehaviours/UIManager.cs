using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UIManager : MonoBehaviour
{
    [Header("Level Panels:")]
    [SerializeField] GameObject levelStart_Panel;
    [SerializeField] GameObject levelComplete_Panel;
    [SerializeField] GameObject levelFailed_Panel;

    [Header("Level Buttons:")]
    [SerializeField] Button nextLevel_Button;
    [SerializeField] Button retryLevel_Button;

    [Header("Texts:")]
    [SerializeField] TMP_Text levelSerial_Text;
    [SerializeField] Vector2 levelHolderPos;

    [Header("Tutorial:")]
    public TMP_Text tutorial_Text;

    // Start is called before the first frame update
    void Start()
    {
        ShowLevelStartUI(GameManager.Instance.CurrentLevel);
        ConfigureUIButtons();
        ConfigureDefaultUI();
    }

    void ConfigureUIButtons()
    {
        nextLevel_Button.onClick.AddListener(() => LevelManager.Instance.GotoNextLevel());
        retryLevel_Button.onClick.AddListener(() => LevelManager.Instance.RetryLevel());
    }

    void ConfigureDefaultUI()
    {
        
    }

    public void ShowLevelStartUI(int level)
    {
        levelSerial_Text.text = level.ToString("00");
        LeanTween.moveLocal(levelSerial_Text.transform.parent.gameObject, levelHolderPos, 0.5f).setEase(LeanTweenType.easeOutCubic);
    }

    public void ShowLevelCompleteUI()
    {
        levelComplete_Panel.SetActive(true);

        LeanTween.delayedCall(nextLevel_Button.gameObject, 1.5f, () =>
        {
            LeanTween.scale(nextLevel_Button.gameObject, Vector3.one * 1.1f, 0.1f)
            .setLoopPingPong(1);
        }).setRepeat(-1);
    }

    public void ShowLeveFailedUI()
    {
        levelFailed_Panel.SetActive(true);
    }
}
