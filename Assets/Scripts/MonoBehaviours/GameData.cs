﻿using System.Collections;
using UnityEngine;


public class GameData : MonoBehaviour
{
    public static GameData Instance { get; private set; }

    public ColorPalette colorPalette;
    
    public ObjectPalette objectPalette;

    public CollectorPalette collectorPalette;

    private void Awake()
    {
        Instance = this;
        Initialize();
    }

    void Initialize()
    {
        colorPalette.Initialize();
        objectPalette.Initialize();
        collectorPalette.Initialize();
    }
}