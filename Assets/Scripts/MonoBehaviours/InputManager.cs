using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Mathematics;
using UnityEngine.EventSystems;

public class InputManager : MonoBehaviour
{
    Vector3 touchStart;
    static Vector3 deltaTouch;
    [SerializeField] float maxDrag;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            touchStart = Input.mousePosition;
        }
        else if (Input.GetMouseButton(0))
        {
            Vector3 currentTouch = Input.mousePosition;
            deltaTouch = currentTouch - touchStart;
            deltaTouch = Vector2.ClampMagnitude(deltaTouch, maxDrag);
        }
        else if (Input.GetMouseButtonUp(0))
        {
            deltaTouch = Vector2.zero;
        }
    }

    public static bool GetTouchDown()
    {
        return !EventSystem.current.IsPointerOverGameObject() && Input.GetMouseButtonDown(0);
    }

    public static float3 GetDrag()
    {
        return deltaTouch;
    }
}
