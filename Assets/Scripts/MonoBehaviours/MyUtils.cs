﻿using System.Collections;
using UnityEngine;

public class MyUtils
{
    [System.Serializable]
    public struct MinMaxInt
    {
        public int min;
        public int max;

        public int GetRandomInRange()
        {
            return Random.Range(min, max + 1);
        }

        public int GetRandomInRange(float biasness)
        {
            float median = Mathf.Lerp(min, max + 1, 0.5f);
            int roundedMedian = (int)median;
            int[] lowerRange = new int[] { min, roundedMedian + 1};
            int[] upperRange = new int[] { roundedMedian + 1, max + 1};

            int dice = Random.Range(1, 101);

            if (dice <= biasness * 100)
            {
                return Random.Range(upperRange[0], upperRange[1]);
            }
            else
            {
                return Random.Range(lowerRange[0], lowerRange[1]);
            }
        }

        public int GetAverage()
        {
            return Mathf.RoundToInt(Mathf.Lerp(min, max + 1, 0.5f));
        }
    }

    [System.Serializable]
    public struct MinMaxFloat
    {
        public float min;
        public float max;

        public float GetRandomInRange()
        {
            return Random.Range(min, max);
        }
    }

    public static Vector3 GetRandomInBox(float l, float w, float h)
    {
        float x = Random.Range(-w / 2f, w / 2f);
        float y = Random.Range(-h / 2f, h / 2f);
        float z = Random.Range(-l / 2f, l / 2f);
        return new Vector3(x, y, z);
    }
}