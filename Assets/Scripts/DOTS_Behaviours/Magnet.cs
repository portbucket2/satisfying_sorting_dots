using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Unity.Entities;
using Unity.Physics;
using Unity.Mathematics;
using Unity.Transforms;
using Unity.Jobs;
using Unity.Physics.Systems;
using Unity.Rendering;

public enum ColorID
{
    Red, Green, Blue
}

public enum ShapeID
{
    Cube, Diamond, Sphere
}

public struct MagnetData : IComponentData
{
    public ColorID colorID;
    public ShapeID shapeID;
    public float moveSpeed;
}

public struct CollectorData : IComponentData
{
    public ColorID colorID;
    public ShapeID shapeID;
}

public struct PhysicalBody : IComponentData
{
    public ColorID colorID;
    public ShapeID shapeID;
    public bool collectable;
    public float3 collectorPos;
}

[UpdateAfter(typeof(EndFramePhysicsSystem))]
public class MagneticSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float3 magnetPos = MagnetManager.Instance.magnetPos;
        bool magnetized = MagnetManager.Instance.magnetized;
        float G_Magnet = MagnetManager.Instance.G_Magnet;
        float G_Collector = MagnetManager.Instance.G_Collector;
        float deltaTime = UnityEngine.Time.deltaTime;
        bool matchBoth = LevelManager.Instance.matchBoth;
        ColorID magnetColorID = MagnetManager.Instance.colorID;
        ShapeID magnetShapeID = MagnetManager.Instance.shapeID;
        JobHandle handle = Entities.WithName("MagnetAttract").
            ForEach((ref PhysicsVelocity physicsVel, in Translation position, in PhysicalBody physicalBody, in PhysicsMass physicsMass) =>
            {
                if (matchBoth)
                {
                    if (physicalBody.colorID == magnetColorID && physicalBody.shapeID == magnetShapeID)
                    {
                        float3 resultantForce = float3.zero;

                        if (magnetized)
                        {
                            float distanceToMagnet = math.distance(position.Value, magnetPos);
                            float3 forceDirMagnet = math.normalize(magnetPos - position.Value);
                            float forceMagMagnet = (G_Magnet * (1f / physicsMass.InverseMass)) / (distanceToMagnet * distanceToMagnet);
                            float3 forceToMagnet = forceMagMagnet * forceDirMagnet;

                            resultantForce = forceToMagnet;
                        }

                        
                        if (physicalBody.collectable)
                        {
                            float distanceToCollector = math.distance(position.Value, physicalBody.collectorPos);
                            float3 forceDirCollector = math.normalize(physicalBody.collectorPos - position.Value);
                            float forceMagCollector = (G_Collector * (1f / physicsMass.InverseMass)) / (distanceToCollector * distanceToCollector);
                            float3 forceToCollector = forceMagCollector * forceDirCollector;

                            resultantForce += forceToCollector;
                        }


                        physicsVel.Linear += physicsMass.InverseMass * resultantForce * deltaTime;
                    }
                }
                else
                {
                    if (physicalBody.colorID == magnetColorID || physicalBody.shapeID == magnetShapeID)
                    {
                        float3 resultantForce = float3.zero;

                        if (magnetized)
                        {
                            float distanceToMagnet = math.distance(position.Value, magnetPos);
                            float3 forceDirMagnet = math.normalize(magnetPos - position.Value);
                            float forceMagMagnet = (G_Magnet * (1f / physicsMass.InverseMass)) / (distanceToMagnet * distanceToMagnet);
                            float3 forceToMagnet = forceMagMagnet * forceDirMagnet;

                            resultantForce = forceToMagnet;
                        }


                        if (physicalBody.collectable)
                        {
                            float distanceToCollector = math.distance(position.Value, physicalBody.collectorPos);
                            float3 forceDirCollector = math.normalize(physicalBody.collectorPos - position.Value);
                            float forceMagCollector = (G_Collector * (1f / physicsMass.InverseMass)) / (distanceToCollector * distanceToCollector);
                            float3 forceToCollector = forceMagCollector * forceDirCollector;

                            resultantForce += forceToCollector;
                        }


                        physicsVel.Linear += physicsMass.InverseMass * resultantForce * deltaTime;
                    }
                }
            }).Schedule(inputDeps);

        handle.Complete();
        return inputDeps;
    }
}

public class MagnetMoveSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        float deltaTime = UnityEngine.Time.deltaTime;
        JobHandle handle = Entities.WithName("MagnetMove").WithoutBurst().
            ForEach((ref PhysicsVelocity physicsVel, in Translation position, in MagnetData magnetData) =>
            {
                physicsVel.Linear = magnetData.moveSpeed * deltaTime * InputManager.GetDrag(); 
                MagnetManager.Instance.magnetPos = position.Value;
            }).Schedule(inputDeps);

        handle.Complete();
        return inputDeps;
    }
}

public class MagnetTransitionSystem : JobComponentSystem
{
    protected override JobHandle OnUpdate(JobHandle inputDeps)
    {
        Entities.WithName("MagnetChange").WithoutBurst().
            ForEach((ref MagnetData magnetData, in RenderMesh mesh) =>
            {

            }).Run();
        return inputDeps;
    }
}